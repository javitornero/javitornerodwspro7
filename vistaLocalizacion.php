<?php
	session_start();
	
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
	
    include_once("controller/Config.php");
    include_once("controller/ControlLocalizacion.php");
?>
<html lang="es">
<?php include_once('web/head.php'); ?>
<body>
	<div class="divCabecera">
	<?php
		include_once('web/divLogo.php');
		if (isset($_SESSION['nom'])) {
			include_once('web/menu.php');
		}
	?>
	</div>
	<div id="contenedor">
		<div class="colOpciones">
		<?php
			if (isset($_SESSION['nom'])) {
				include_once('web/divNomUsuario.php');
				include_once('web/divMod.php');
			}
		?>		
		</div>
		<div class="contenido">
			<?php

			$controladorLoc= new ControlLocalizacion();

			//**********************************
			// Funciones para el CREATE

			if (isset($_REQUEST["sbCreateLoc"])) {
				$id = recoge("id");
				$nom = recoge("nom");
				if ($id!="" && $nom!="") {				
					echo $controladorLoc->createLoc($id, $nom);
				} else {
					echo "Operación no realizada, se han encontrado campos vacíos.";
				}
			}

			//**********************************
			// Funciones para el READ

			// Función que pinta una tabla con una vista de los objetos Localizaciones
			function pintaTablaLoc($controladorLoc){
				$arrayObjs=$controladorLoc->readLoc();
				echo "<table>";
				echo "<tr class='cabTabla'><td class='colTabla'>Id</td><td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;Nombre</td><td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;Usuarios</td></tr>";
				if (is_array($arrayObjs)) { // Si es un array nos llegan todas las localizaciones y las pintamos
					$row=count($arrayObjs);
					for($i=0;$i<$row;$i++){
						echo "<tr class='filaTabla'>";
						echo "<td class='colTabla'>".$arrayObjs[$i]->getId()."</td>";
						echo "<td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;".$arrayObjs[$i]->getNom()."</td>";
						echo "<td class='colTabla'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$controladorLoc->countUsuariosByLoc($arrayObjs[$i])."</td>";
						echo "</tr>";
					}
				}else{ // Si no es un array es que devuelve un mensaje de error
					echo $controladorLoc->readLoc();
				}
				echo "</table>";
			}
			if (isset($_REQUEST["sbReadLoc"])) { ?>
				<div class="divReadRes" id="divReadLoc">
					<h1>Localizaciones</h1>
					<div class="panelScroll">
						<?php pintaTablaLoc($controladorLoc); ?>
					</div>
				</div>
			<?php }

			//**********************************
			// Funciones para el UPDATE

			if (isset($_REQUEST["sbUpdateLoc"])) {
				$id = recoge("id");
				$nom = recoge("nom");
				if ($id!="" && $nom!="") {
					echo $controladorLoc->updateLoc($id, $nom);				
				} else {				
					echo "Operación no realizada, se han encontrado campos vacíos.";
				}
			}

			//**********************************
			// Funciones para el DELETE

			//////////////////////////////////////////////////////////////////////////

			if (isset($_REQUEST["sbDeleteLoc"])) {
				$id = recoge("id");
				if ($id!="") {
					echo $controladorLoc->deleteLoc($id);				
				} else {				
					echo "Operación no realizada, se han encontrado campos vacíos.";
				}
			}

			?>
		</div>
	</div>
	<footer>
		<?php include_once('web/pie.php'); ?>		
	</footer>
</body>
</html>