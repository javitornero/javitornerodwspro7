<?php
	session_start();

	error_reporting(E_ALL);
	ini_set('display_errors', '1');

    include_once("controller/Config.php");
    include_once("controller/Controlador.php");

?>
<html lang="es">
<?php include_once('web/head.php'); ?>
<body>
	<div class="divCabecera">
			<?php include_once('web/divLogo.php'); ?>
	</div>
	<div id="contenedor">
		<div class="colOpciones"></div>
		<div class="contenido">
		<?php
			if (isset($_REQUEST['nom']) && isset($_REQUEST['password'])) {				
				$nom = recoge("nom");
				$password = recoge("password");
				$controlador= new Controlador();
				echo $controlador->validaAdministrador($nom, $password);
			}else if (isset($_REQUEST['sbCierraSesion'])) {
				unset($_SESSION["nom"]);
				unset($_SESSION["modelo"]);
				session_destroy();
				echo "Ha cerrado la sesión";
			}
		?>
		<br><a href="index.php">Aceptar</a>
		</div>
	</div>
	<footer>
		<?php include_once('web/pie.php'); ?>		
	</footer>
</body>
</html>