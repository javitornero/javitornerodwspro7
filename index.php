<?php
	session_start();

	error_reporting(E_ALL);
	ini_set('display_errors', '1');


    include_once("controller/Config.php");

    include_once("utils/utils_index.php");
	inicializaModelo(); // Inicializa el modelo con valor mysql
	cambiaModelo(); // Escucha cuando se elije un modelo distinto y lo guarda en variable sesión

    include_once("controller/Controlador.php");
    include_once("controller/ControlLocalizacion.php");
    include_once("controller/ControlUsuario.php");
    
	inicializaModelo(); // Inicializa el modelo con valor mysql
	cambiaModelo(); // Escucha cuando se elije un modelo distinto y lo guarda en variable sesión

	$controlador= new Controlador();
    $controladorLoc= new ControlLocalizacion();
    $controladorU= new ControlUsuario();

	$testConexion=$controlador->conectar();

?>
<html lang="es">
<?php include_once('web/head.php'); ?>
<body>
	<div class="divCabecera">
	<?php
		include_once('web/divLogo.php');
		if ($_SESSION["modelo"]!="" && $testConexion=='OK' && isset($_SESSION['nom'])) { include_once('web/menu.php'); }
	?>
	</div>
	<div id="contenedor">
		<div class="colOpciones">
		<?php
			if ($_SESSION["modelo"]!="" && $testConexion=='OK' && isset($_SESSION['nom'])) {
				include_once('web/divNomUsuario.php');
				include_once('web/divMod.php');
			}
		?>
		</div>
		<div class="contenido">
		<?php
			 if ($testConexion!='OK') { // Si el test de conexión no nos devuelve OK ofrecemos instalación
				if (!isset($_REQUEST["instalar"])) { // Compueba si no existe la variable para instalar
					echo $testConexion; // Mostramos el mensaje de error SQL que nos devuelve el intento de conexión
					include_once('templates/formInstalar.php'); // y mostramos el formulario que permite la instalación
				}else{
					echo $controlador->instalar(); // Si existe la variable para instalar nos devuelve mensaje de éxito o error
				}
			}else if (!isset($_SESSION["nom"])) { // Si no estas logueado te muestro SOLO el formulario de loguin
				include_once('templates/formLogin.php');
			}else if ($_SESSION["modelo"]=="") { // Si no hay modelo activo lo pìdo y no doy mas opciones
				echo "Elige modelo.";
			} else{ // Si ha llegado hasta aquí es porque el login es OK, se ha seleccionado un modelo válido y la prueba de conexión es OK
				if ($_SESSION["modelo"]=="mysql") { $controlador->desconectar();} // Test de conexión OK, cerramos conexión a la BD
				if (isset($_GET["show"])) {
					switch ($_GET["show"]) {
						case 'addLoc':
						include_once('templates/localizacionCreate.php');
						break;
						case 'showLoc':
						include_once('templates/localizacionRead.php');
						break;
						case 'upDateLoc':
						include_once('templates/localizacionUpdate.php');
						break;
						case 'delLoc':
						include_once('templates/localizacionDelete.php');
						break;
						case 'addU':
						include_once('templates/usuarioCreate.php');
						break;
						case 'showU':
						include_once('templates/usuarioRead.php');
						break;
						case 'upDateU':
						include_once('templates/usuarioUpdate.php');
						break;
						case 'delU':
						include_once('templates/usuarioDelete.php');
						break;
					}
				}			
			}
		?>
		</div>
	</div>
	<footer>
		<?php include_once('web/pie.php'); ?>		
	</footer>
</body>
</html>