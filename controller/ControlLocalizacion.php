<?php
	include_once('utils/utils_control.php');
    include_once("./model/Localizacion.php");
/**
* 
*/
class ControlLocalizacion {

	public function createLoc($id, $nom) {
		$localizacion = new Localizacion($id, $nom);
		$modelo=obtenerModelo();
		return $modelo->createLocalizacion($localizacion); // Añadimos los datos del nuevo objeto
	}
	
	public function readLoc() {
		$modelo=obtenerModelo();
		return $modelo->readLocalizaciones(); // Pedimos todos los localizaciones al modelo y nos lo devuelve en un array de objetos
	}
	
	public function countUsuariosByLoc($localizacion) {
		$modelo=obtenerModelo();
		return $modelo->countUsuariosByLoc($localizacion); // Devuelve cantidad de usuarios que se encuentran en la localización dada
	}

	public function updateLoc($id, $nom) {
		$localizacion = new Localizacion($id, $nom);
		$modelo=obtenerModelo();
		return $modelo->updateLocalizacion($localizacion); // Actualizamos los datos del nuevo objeto y devuelve string de hecho o error
	}

	public function deleteLoc($id) {
		$localizacion = new Localizacion($id, "");
		$modelo=obtenerModelo();
		return $modelo->deleteLocalizacion($localizacion); // Eliminamos los datos del nuevo objeto y devuelve string de hecho o error
	}
}

?>