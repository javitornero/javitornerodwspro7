<?php
	include_once('utils/utils_control.php');
	include_once("./model/Usuario.php");
	include_once("./model/Localizacion.php");
/**
* 
*/
class ControlUsuario {

	public function createU($id, $nom, $id_loc) {
		$localizacion = new Localizacion($id_loc, "");
		$usuario = new Usuario($id, $nom, $localizacion);
		$modelo=obtenerModelo();
		return $modelo->createUsuario($usuario); // Añadimos los datos del nuevo objeto y devuelve string de hecho o error
	}
	public function readU() {
		$modelo=obtenerModelo();
		return $modelo->readUsuarios(); // Pedimos todos los usuarios al modelo y nos lo devuelve en un array de objetos
	}

	public function updateU($id, $nom, $id_loc) {
		$localizacion = new Localizacion($id_loc, "");
		$usuario = new Usuario($id, $nom, $localizacion);
		$modelo=obtenerModelo();
		return $modelo->updateUsuario($usuario); // Actualizamos los datos del nuevo objeto y devuelve string de hecho o error
	}

	public function deleteU($id) {
		$localizacion = new Localizacion("", "");
		$usuario = new Usuario($id, "", $localizacion);
		$modelo=obtenerModelo();
		return $modelo->deleteUsuario($usuario); // Eliminamos los datos del nuevo objeto y devuelve string de hecho o error
	}
}

?>