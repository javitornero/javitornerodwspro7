<?php
	include_once('utils/utils_control.php');
    include_once("./model/Administrador.php");
    include_once("./model/Localizacion.php");
/**
* 
*/
    class Controlador{

		function conectar(){
			if ($_SESSION["modelo"]=="fichero") {
				$respuesta= "OK"; // Devuelvo OK porque los métodos de instalación, conexión y desconexión los veo necesarios solo para el modelo Mysql
			}else if($_SESSION["modelo"]=="mysql"){
				$modelo=obtenerModelo();
				$respuesta= $modelo->conectar();
			}
			return $respuesta;
		}

		function desconectar(){
			$modelo=obtenerModelo();
			return $modelo->desconectar();
		}

		function instalar(){
			$modelo=obtenerModelo();
			$respuesta= $modelo->instalar(Config::$bdnombre, Config::$bdusuario, Config::$bdclave, Config::$bdhostname);
			$respuesta= $respuesta. "<br><a class='aOK' href='index.php'>OK</a>";
			return $respuesta;
		}

		function validaAdministrador($nom, $password){
			$administrador=new Administrador("", $nom, $password, "");
			if ($_SESSION["modelo"]=="mysql") {
				$modelo=obtenerModelo();
				$administrador= $modelo->traeAdministrador($administrador);
			}
			if ($administrador->getNom()) {
				$_SESSION["nom"] = $administrador->getNom();
				return "Bienvenido ".$_SESSION['nom'];
			}else{
				return "Datos incorrectos. Revise los campos de entrada.";
			}
		}

		function newId($soporte){
			$modelo=obtenerModelo();
			return $modelo->newId($soporte);
		}

    }
?>