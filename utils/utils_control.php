<?php

// Función que limpia la entrada de datos en formularios
function recoge($campo) { 
	if (isset($_REQUEST[$campo])) {
		$valor = htmlspecialchars(trim(strip_tags($_REQUEST[$campo])));
	} else {
		$valor = "";
	};
	return $valor;
}

function obtenerModelo() {
	if (isset($_SESSION["modelo"])) {
		$tipo = $_SESSION["modelo"];
		switch ($tipo) {
			case 'fichero':
				include_once("./model/ModeloFichero.php");
				$modelo = new ModeloFichero();
				break;
			case 'mysql':
				include_once("./model/ModeloMysql.php");
				$modelo = new ModeloMysql(Config::$bdnombre, Config::$bdusuario, Config::$bdclave, Config::$bdhostname);
				break;
		}
		return $modelo;
	}else{
		echo "Error : No hay modelo";
	}
}

?>