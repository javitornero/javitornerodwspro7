<?php

function inicializaModelo(){
	if (!isset($_SESSION["modelo"])) {
		$_SESSION["modelo"]="mysql";
	}
}

function cambiaModelo(){
	if (isset($_REQUEST['selectModelo'])) {
		if ($_REQUEST['selectModelo']=="fichero" || $_REQUEST['selectModelo']=="mysql") {
			$_SESSION["modelo"]=$_REQUEST['selectModelo'];
		}
	}
}

function pintaOptions($arrayObjs){
	if ($arrayObjs) {
		echo "<option selected disabled>Elige opción</option>";
	} else{
		echo "<option selected disabled>No hay localizaciones creadas.</option>";	
	}
	for($i=0;$i<count($arrayObjs);$i++){
		echo "<option value=".$arrayObjs[$i]->getId().">id: ".$arrayObjs[$i]->getId()." - ".$arrayObjs[$i]->getNom()."</option>";
	}
}

?>